<?php
    $cipher = "aes-256-gcm";
    
    if (in_array($cipher, openssl_get_cipher_methods()))
    {
        echo "openssl version text:" . OPENSSL_VERSION_TEXT . "\n";
        echo "openssl version number:" . OPENSSL_VERSION_NUMBER . "\n";
        //var_dump(openssl_get_cipher_methods());
    }
    else {
        die($cipher . "not in open ssl library.");
    }

    class Email {
        public $to;
        public $from;
        public $msg;
    }

    class Block {
        public $previous_hash;
        public $timestamp;
        public $email;

        function __construct() {
            $this->email = new Email;
        }
    }
    
    $genesis_block = new Block;
    $genesis_block->previous_hash = "";
    $genesis_block->timestamp = "";
    $genesis_block->email->to = "bob@mailinator.com";
    $genesis_block->email->from = "terry@mailinator.com";
    $genesis_block->email->msg = "This is a message to be encrypted.";

    $blocks[] = $genesis_block;
    $hash = openssl_random_pseudo_bytes(256);
    $tag = 0;
    $ivlen = openssl_cipher_iv_length($cipher);
    $iv = openssl_random_pseudo_bytes($ivlen);
    $data = json_encode($genesis_block);
    $current_hash = openssl_encrypt($data, $cipher, $hash, $options=0, $iv, $tag);
    
    //construction of the blockchain.
    $blocks = array();
    $blocks[] = $current_hash;

    for ($i=0; $i <= 5; $i++) {
        $block = new Block;
        $block->email->to = "To {$i} user.";
        $block->email->from = "From {$i} user.";
        $block->email->msg = "{$i} This is a message to be encrypted.";
        $block->timestamp = "";
        $block->previous_hash = $current_hash;
        $data = json_encode($block);
        $last_hash = $current_hash;

        echo "encrypt";
        var_dump($current_hash);
        
        try {
            $current_hash = openssl_encrypt($data, $cipher, $current_hash, $options=0, $iv, $tag);
        }
        catch (Exception $e) {
            var_dump($e);
            die();
        }

        if ($current_hash === NULL) {
            die('Encryption did not work.');
        }

        $blocks[] = $current_hash;
    }
    
    //var_dump($blocks);
    //die();

    $block_chain_length = sizeof($blocks) - 1;
    $current_hash = $last_hash;

    for ($i = $block_chain_length; $i >= 0; $i--) {
        $block = $blocks[$i];
        echo "decrypt";
        var_dump($current_hash);
        $decrypted_block = openssl_decrypt($block, $cipher, $current_hash, $options=0, $iv, $tag);
        $decrypted_block = json_decode($decrypted_block);
        $current_hash = $decrypted_block->previous_hash;
    }
?>