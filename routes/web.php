<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('blockview', ['name' => 'James']);
});

$router->get('/decrypt', function () use ($router) {
    return view('decrypt_blockview');
});

$router->post('encryptjson/{id}', 'EncryptController@GenerateEncodedMessage');
$router->post('encrypt/{id}', 'EncryptController@GenerateKeys');
$router->get('decrypt/{id}', 'EncryptController@GetEmails');
$router->post('decrypt_message', 'EncryptController@DecryptMessage');
$router->get('displayblock', function () use ($router) {
    return "This is a computed thing.";
});

$router->get('genInitVectorJSON', function () use ($router) {
    $key = openssl_random_pseudo_bytes(256);
    $plaintext = "message to be encrypted";
    $cipher = "aes-256-gcm";
    $tag = 0;
    if (in_array($cipher, openssl_get_cipher_methods()))
    {
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv, $tag);        
        $original_plaintext = openssl_decrypt($ciphertext, $cipher, $key, $options=0, $iv, $tag);
        echo json_encode($original_plaintext);
    }
});

$router->get('genInitVector', function () use ($router) {
   
    class Email {
        private $to;
        private $from;
        private $msg;
    }
    
    class Block {
        private $prev_key;
        private $timestamp;
        private $data;
        private $key;
    
        function __construct() {
            $this->data = new Email;
        }
    }
    
    /*
    class Block:
    def __init__(self, index, timestamp, data, previous_hash):
      self.index = index
      self.timestamp = timestamp
      self.data = data
      self.previous_hash = previous_hash
      self.hash = self.hash_block()
    
    def hash_block(self):
      sha = hasher.sha256()
      sha.update(str(self.index) + 
                 str(self.timestamp) + 
                 str(self.data) + 
                 str(self.previous_hash))
      return sha.hexdigest()
    */
    
    function Genesis() {
        $block = new Block;
        $block->email->to = "To {$i} user.";
        $block->$from = "From {$i} user.";
    
    }
    
    //$key should have been previously generated in a cryptographically safe way, like openssl_random_pseudo_bytes
    $key = openssl_random_pseudo_bytes(256);
    $plaintext = "message to be encrypted";
    $cipher = "aes-256-gcm";
    $tag = 0;
    //echo "openssl version text: " . OPENSSL_VERSION_TEXT . "\n";
    //echo "openssl version number:  " . OPENSSL_VERSION_NUMBER . "\n";
    //var_dump(openssl_get_cipher_methods());
    if (in_array($cipher, openssl_get_cipher_methods()))
    {
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv, $tag);
        //store $cipher, $iv, and $tag for decryption later
        for($i=0; $i < strlen($tag); $i++) {
            printf("\\x%02X", ord($tag[$i]));
        }
        
        $original_plaintext = openssl_decrypt($ciphertext, $cipher, $key, $options=0, $iv, $tag);
        //echo $original_plaintext."\n";
    }
    
    /*for ($i=0; $i <=5; $i++) {
        $block = new Block;
        //$block->email->to = "To {$i} user.";
        //$block->$from = "From {$i} user.";
    
    }*/
    

    
});


$router->get('fakeEmail', function () use ($router) {
    return uniqid() . '@noreply.com';
});

$router->get('getPrivateKey', function () use ($router) {

	$file = 'files/key.private';
	
	if (file_exists($file)) {
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit();
	}
	else {
	   echo "error";
        }
});

$router->get('keyPair', function () use ($router) {
        //TEST
        $keypair = openssl_pkey_new();
        //var_dump($keypair);
        openssl_pkey_export($keypair, $privkey);
        //var_dump($privkey);
        openssl_pkey_export_to_file($privkey, 'files/key.private');


        // Get public key
        $pubkey=openssl_pkey_get_details($keypair);
        $pubkey=$pubkey["key"];
        echo $pubkey;
});

$router->post('block', function () use ($router) {
    class Email {
        private $to;
        private $from;
        private $msg;
    }
    
    class Block {
        private $prev_key;
        private $timestamp;
        private $data;
        private $key;
    
        function __construct() {
            $this->data = new Email;
        }
    }
    
    function Genesis() {
        $block = new Block;
        $block->email->to = "To {$i} user.";
        $block->$from = "From {$i} user.";
    
    }
    
    function pd() {
        echo '<pre>';
        print_r (var_export($_POST, true));
        echo '</pre>';
    }
    
    
        $key = openssl_random_pseudo_bytes(256);
        $cipher = "aes-256-gcm";
        $tag = 0;
    
        $data = $_POST['data'];
    
        $block = [
            $data['fromEmail'],
            $data['toEmail'],
            $data['email']
        ];
    
        $key = $data['publicKey'];
        $iv = $data['iv'];
        $block = serialize($block);
    
        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            //$ivlen = openssl_cipher_iv_length($cipher);
            //$iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext = openssl_encrypt($block, $cipher, $key, $options=0, $iv, $tag);
            echo $ciphertext;
    
            //$original_plaintext = openssl_decrypt($ciphertext, $cipher, $key, $options=0, $iv, $tag);
            //echo $original_plaintext;
    
        }
    
        //$block = new Block;
    
        //var_dump($block);
    
});

/*
$router->get('/', function () use ($router) {
    return view('blockview', ['name' => 'James']);
});

$router->get('/', function () use ($router) {
    return view('blockview', ['name' => 'James']);
});
*/
