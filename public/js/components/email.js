                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_from_email">From: *</label>
                                    <input id="init-from-email" v-model="fromEmail" type="text" name="from-email" class="form-control" placeholder="Please enter fake email address *" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_to_email">To: *</label>
                                    <input id="init-to-email" v-model="toEmail" type="email" name="to-email" class="form-control" placeholder="Please enter fake email address *" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Time Stamp</label>
                                    <input id="timestamp" v-model="datenow" type="text" name="iv" class="form-control" required="required" data-error="Timestamp is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

