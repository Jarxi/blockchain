var vm = new Vue({
    el: '#app',
    data: {
        //
        initialVector: '',
        latinTxt: 'Haec est magna nuntium. Ego magnam computer. Fortis ego sum. Equus amicitia.',
        fromEmail: '',
        toEmail: '',
        publicKey: '',
        newBlock: '',
        datenow: '',
        testblock: 'test block',
        displayBlock: 0,
        /*message: '<p> This website was written with PHP and the Lumen framework with Vue.js.<br>I have been programming for over 30 years. I am for hire but not for the usual office agile bullshit. <br>Remote only with occaisional office visits :-) <br>I make beautiful elegant bespoke websites for the connoisseur. This website lets you encrypt a message in the safest way possible - you can generate a key or use your own.<br><strong>neoarbuckle@gmail.com</strong></p>'*/
        message: '<p> This website was written with PHP and the Lumen framework with Vue.js.<br><strong>neoarbuckle@gmail.com</strong></p>'
    },
    computed: {
        /*showBlock: function() {
            this.$http.get('/displayblock').then(
                response => {
                    return response.body;
                }, 
                response => {
                    alert('Error!');
                }
            );
        }*/
        showBlock: function() {
            return this.message;
        }
    },
    mounted: function () {
        /*
        this.$nextTick(function () {
          // Code that will run only after the
          // entire view has been rendered
        })
        */
        //this.time()
        setInterval(this.time, 1000)
        this.$http.get('/displayblock').then(
            response => {
                return response.body;
            }, 
            response => {
                alert('Error!');
            }
        );
    },
    methods: {
        time() {
            this.datenow = moment().format('MMMM Do YYYY, h:mm:ss a');
            /*this.datenow = Date.now().toString()*/
        },
        genInitVector() {
            this.$http.get('/genInitVector').then(
                response => {
                    this.initialVector = response.body;
                }, 
                response => {
                    alert('Error!');
                }
            );
            this.$http.get('/fakeEmail').then(
                response => {
                    this.fromEmail = response.body;
                }, 
                response => {
                    alert('Error!');
                }
            );
            this.$http.get('/fakeEmail').then(
                response => {
                    this.toEmail = response.body;
                }, 
                response => {
                    alert('Error!');
                }
            );
            this.$http.get('/keyPair').then(
                response => {
                    this.publicKey = response.body;
                }, 
                response => {
                    alert('Error!');
                }
            );
        },
        createBlock(event) {
            this.$data.displayBlock = 1;
            //this.$http.post('/block',
            this.$http.post('/encrypt/1',
                { data: {
                        fromEmail: this.$data.fromEmail,
                        toEmail: this.$data.toEmail,
                        publicKey: this.$data.publicKey,
                        email: this.$data.latinTxt,
                        iv: this.$data.initialVector,
                        timeStamp: this.$data.datenow
                    }
                },
                {
                    emulateJSON: true
                }).then(
                response => {
                    this.newBlock = response.body;
                }, 
                response => {
                    alert('Error!');
                })
        },
    },
})
