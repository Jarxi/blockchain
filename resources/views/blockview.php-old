    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
		<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
		<link rel="manifest" href="/favicons/site.webmanifest">
		<link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="css/w3.css">
            <body class="w3-content">
    
            <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
            <link type="text/css" rel="stylesheet" href="css/bootstrap-vue.css"/>
        
            <script src="js/vue.js"></script>
            <script src="js/polyfill.min.js"></script>
            <script src="js/bootstrap-vue.js"></script>
            <script src="js/vue-resource@1.3.4"></script>
            <script src="js/moment.js"></script>
            <script>
    
                var block_id = 0;
    
                var BlockComponent = Vue.extend({
                        template: `<div class="row">
                                <div class="col-md-12">
                                    <label for="form_key"><span style="color:red">New Block</span></label>
                                    <textarea :id="blockID" type="tel" name="key" class="form-control" placeholder="The new block is printed here." rows="4" required="required" data-error="You forgot your key."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>`,
                        data: function() {
                            return {
                                text: "This is a new block.",
                                blockID: "block-id-" + block_id++
                            }
                        }
                })
                
                Vue.component('block-component', BlockComponent)
                
           </script>
    
           <style>
               #app {
                   margin: 50px;
               }
           </style>
    
        </head>
    
        <body>
            <div id="block-generator" method="post" role="form">
                <div id="app">
	  	<h1>Encrypt your email message!</h1>
                    <span v-html="showBlock"></span>                
                    <div class="messages"></div>
                
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Blockchain Illustration by <a href="https://github.com/jwent" target="_blank">Jeremy Went</a><br>This website is hosted on my PC at home. Sometimes I turn it off.</p>
                                <p>Blocks are encrypted using Open SSL <a href="https://en.wikipedia.org/wiki/Galois/Counter_Mode" target="_blank">AES 256 GCM</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_name">Initial Vector *</label>
                                    <input id="init-vector" v-model="initialVector" type="text" name="iv" class="form-control" placeholder="Please hit button to generate *" required="required" data-error="Vector is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input id="genInitVectorBtn" v-on:click.prevent="genInitVector" type="submit" class="btn btn-success btn-send" value="Create IV">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_from_email">From: *</label>
                                    <input id="init-from-email" v-model="fromEmail" type="text" name="from-email" class="form-control" placeholder="Please enter fake email address *" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_to_email">To: *</label>
                                    <input id="init-to-email" v-model="toEmail" type="email" name="to-email" class="form-control" placeholder="Please enter fake email address *" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Time Stamp</label>
                                    <input id="timestamp" v-model="datenow" type="text" name="iv" class="form-control" required="required" data-error="Timestamp is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="key-text-area" class="form-group">
                                    <label for="form_key">Public Key *</label>
                                    <textarea @focus="$event.target.select()" id="public-key" v-model="publicKey" type="tel" name="key" class="form-control" placeholder="Please enter your key *" rows="4" required="required" data-error="You forgot your key."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Message *</label>
                                    <textarea id="email-body" v-model="latinTxt" name="message" class="form-control" placeholder="Email Message *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p class="text-muted"><strong>*</strong> These fields are required.</p>
                            </div>
                            <div class="col-md-12">
                                <input v-on:click="createBlock" type="submit" class="btn btn-success btn-send" value="Create Block">
                            </div>
                        </div>
                    </div>
    
                    <!--Block chain goes here. -->
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="form_key"><span style="color:red">New Block</span></label>
                            <textarea id="new-block" v-model="newBlock" type="tel" name="key" class="form-control" placeholder="The new block is printed here." rows="4" required="required" data-error="You forgot your key."></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
<!-- An attempt to generate blocks etc but I think we will turn this site into an encrypted message thing... -->
                    <!--div>
                        <block-component v-if="displayBlock"></block-component>
                        <block-component v-if="displayBlock"></block-component>
                    </div-->
                
                </div>
            </div>
    
        </body>
        <script src="js/local.js"></script>
    </html>
