<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Helpers
{
    public static function dd($d) 
    {
        echo '<pre>';
        print var_export($d, true);
        echo '</pre>';
        die();
    }

}

class EncryptController extends Controller
{
    /**
     * Generate public/private key pair and store public key in database for id.
     *
     * @return private key.
     */
    public function GenerateEncodedMessage(Request $request, $id)
    {
        $key_pair = openssl_pkey_new();
        openssl_pkey_export($key_pair, $private_key);
        $key_data = openssl_pkey_get_details($key_pair);
        $public_key = $key_data["key"];
        $data = $request->all();
        $key = openssl_random_pseudo_bytes(256);
        $cipher = "aes-256-gcm";
        $tag = 0;

        $block = [
            $data['fromEmail'],
            $data['toEmail'],
            $data['timeStamp'],
            $data['email']
        ];
    
        $key = $data['publicKey'];
        $iv = $data['iv'];
        $block = serialize($block);
    
        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $ciphertext = openssl_encrypt($block, $cipher, $key, $options=0, $iv, $tag);
            return json_encode($ciphertext);
        }
        
        return false;
    }

    public function GenerateKeys(Request $request, $id)
    {
        $key_pair = openssl_pkey_new();
        openssl_pkey_export($key_pair, $private_key);
	openssl_pkey_export_to_file($private_key, 'key_' . uniqid() . '.private');
        $key_data = openssl_pkey_get_details($key_pair);
        $public_key = $key_data["key"];
       
        $data = $request->all();

	//$_debug_data = var_export($data, true);
	//return $_debug_data;

        //$data = array_values((array)json_decode($data[0]));
	$data = $data['data'];
        //Helpers::dd($data);


        $key = openssl_random_pseudo_bytes(256);
        $cipher = "aes-256-gcm";
        $tag = 0;

        $block = [
            $data['fromEmail'],
            $data['toEmail'],
            $data['timeStamp'],
            $data['email']
        ];
    
        $key = $data['publicKey'];
        $iv = $data['iv'];
        $block = serialize($block);
    
        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $ciphertext = openssl_encrypt($block, $cipher, $key, $options=0, $iv, $tag);

/*
            try {
                $results = app('db')->select(
                    "INSERT INTO blocks
                        (`iv`, `from`, `to`, `time`, `message`, `public_key`)
                        VALUES (
                                '{$data['iv']}',
                                '{$data['fromEmail']}',
                                '{$data['toEmail']}',
                                '{$data['timeStamp']}',
                                '{$ciphertext}',
                                '{$data['publicKey']}');");
            }
            catch(Exception $e) {
                die($e->message());
            }
 */   
            return $ciphertext;
        }
        
        return false;
    }

    public function GetEmails(Request $request, $id) {
        try {
            $results = app('db')->select(
                "select * from blocks LIMIT 1;");
        }
        catch(Exception $e) {
            die($e->message());
        }

        return $results;
       
    }

    public function DecryptMessage(Request $request, $id) {
    }
}
